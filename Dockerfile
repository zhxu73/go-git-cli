# syntax=docker/dockerfile:1
FROM golang:1.16 as build
COPY . /go-git-cli
WORKDIR /go-git-cli
RUN go build -o go-git-cli

FROM gcr.io/distroless/base-debian10
COPY --from=build /go-git-cli/go-git-cli /
CMD ["/go-git-cli"]
