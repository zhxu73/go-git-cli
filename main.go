package main

import (
	"fmt"
	"os"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("go-git-cli [gitURL] [directory]")
		os.Exit(1)
	}
	gitURL := os.Args[1]
	dir := os.Args[2]

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.Mkdir(dir, os.ModePerm)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
	}

	var gitAuth transport.AuthMethod
	username, ok := os.LookupEnv("GIT_USERNAME")
	if ok {
		password, ok := os.LookupEnv("GIT_PASSWORD")
		if !ok {
			fmt.Fprintln(os.Stderr, "missing GIT_PASSWORD")
			os.Exit(1)
		}
		gitAuth = &http.BasicAuth{
			Username: username,
			Password: password,
		}
	}

	_, err := git.PlainClone(dir, false, &git.CloneOptions{
		URL:  gitURL,
		Auth: gitAuth,
	})
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		if err.Error() == "authentication required" {
			fmt.Println("export GIT_USERNAME=XXX")
			fmt.Println("export GIT_PASSWORD=XXX")
		}
		os.RemoveAll(dir)
		os.Exit(1)
	}
}
